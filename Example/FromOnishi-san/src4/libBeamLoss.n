(* Entry point *)

Nb[bc_]:=Module[{},
  circ=LINE["S","$$$"]-(Twiss["DZ","$$$"]-Twiss["DZ","^^^"]);
  f0=SpeedOfLight/circ;
  bc/(ElectronCharge*f0)
  ];

BeamLoss=Class[{},{},
  {
    file,head,v1,v2,v3,rawdata,
    pscat,lscat,lsum,nscat,pobs,lobs,nobs,
    delta,pvac,lossp,loss,totloss,
    beamgas=False,Nb,physProc="C",accept,pave=1e-9,
    lostParticles,pz={},
    norm=True,tfac=1 (* 1 event = 2 particles loss *)
    },

  Constructor[]:=Module[{},
    (* Read data file *)

    f=OpenRead[file];

    head=Read[f];
    v1=Read[f];
    v2=Read[f];
    v3=Read[f];
    accept=Read[f];
    Close[f];

    rawdata=v1;

    pscat=v2[[,1]];
    lscat=v2[[,2]];
    lsum=Plus@@lscat;
    nscat=Length[pscat];

    pobs=v3[[,1]];
    lobs=v3[[,2]];
    nobs=Length[pobs];

    If[physProc=="C",delta=head[[3,2]]/head[[3,1]];beamgas=True];
    If[physProc=="B",delta=(1-head[[3,2]])/head[[3,1]];beamgas=True];
    If[physProc=="T",delta=(1-head[[3,2]])/head[[3,1]];beamgas=False];

    (* Set vacuum level *)
    (* 1 Torr = 101325/760 Pa *)
    pvac=Table[Torr2Pa[pave],{nscat}];

    ];

  Torr2Pa[v_]:=(v*101325/760);

  ng[p_:1e-7]:=Module[{T=300,kb=1.38e-23},
    2*p/kb/T
    ];

(* Sum of lost particles *)
  CalcLoss[]:=Module[{a},
    pvac=Table[Torr2Pa[pave],{nscat}];
    vv1={};
    Do[
      (* weight of length *)
      lc=lscat[[i]]/lsum;
      If[beamgas,
!        vv1[[i]]=Thread[{v1[[i,,1]],SpeedOfLight*ng[pvac[[i]]]*lc*v1[[i,,2]]}];
        AppendTo[vv1,Thread[{v1[[i,,1]],SpeedOfLight*ng[pvac[[i]]]*lc*v1[[i,,2]]}]];
        ,
        AppendTo[vv1,Thread[{v1[[i,,1]],tfac*SpeedOfLight*PBUNCH*lc*v1[[i,,2]]}]];
        ];
      ,{i,1,nscat}];
    
    vv1=Sort[Flatten[vv1,1],(#1[[1]]<#2[[1]])&];
    lossp=Union[vv1[[,1]]];
    lossloc=Sort[Union[LINE["S",losp]]];

    loss={};
    Do[
      a=Select[vv1,(#[[1]]==pobs[[i]])&];
      If[a<>{},
        AppendTo[loss,{pobs[[i]],Plus@@a[[,2]]}];
        ,
        AppendTo[loss,{pobs[[i]],0}];
        ];
      ,{i,1,Length[pobs]}];
    totloss=Plus@@loss[[,2]];

    ];

  op[from_:"^^^",to_:"$$$",nm_:"QD1*|IP.1|*MID|INJECTIO",sc_:Log]:=Module[
    {
      ss,dat,ymax=Automatic,ymin=Automatic,g,ymax2=Automatic,t="",a,ss0,b,c,d={},fileName
      },
    fileName=file[StringPosition[file,"/"][[-1,1]]+1,-1];
    ss0=LINE["S",loss[[,1]]];
    If[norm,
      t="/m";
      r1=loss[[,2]]/lobs;
      ,
      r1=loss[[,2]];
      ];

    (* Merge same locations s *)
    ss=Sort[Union[ss0]];
    a=Thread[{ss0,r1}];
    Do[
      p=Position[a,ss[[i]]][[,1]];
      b=a[[#]]&/@p;
      c=Plus@@b[[,2]];
      AppendTo[d,{ss[[i]],c}];      
      ,{i,1,Length[ss]}];
    ss=d[[,1]];
    r1=d[[,2]];
    r2=PBUNCH*r1;
    If[sc===Log,
      dat=Select[r1,(#>0)&];
      ymax=Max[dat]*10;
      ymin=Min[dat]*0.1;
      ymax2=PBUNCH*ymax;
      ];

    r1=Thread[{ss,r1}];
    r2=Thread[{ss,r2}];
    g=OpticsPlot[{
      {"BX","BY"},{"EX","EY"},
      {r1,PlotJoined->Step,StepRatio->0,Scale->sc,PlotRange->{ymin,ymax},
        FrameLabel->"Rate (1/s"//t//")"},
      {r2,PlotJoined->Step,StepRatio->0,Scale->sc,PlotRange->{1,ymax2},
        FrameLabel->"PPS"//t}
      },
      ListCoordinate->"S",Region->{from,to},Names->nm,PlotLabel->fileName];
    Update[]
    ];

  pla[from_:"^^^",to_:"$$$",nm_:"QD1*|IP.1|*MID|INJECTIO"]:=Module[{ss},
    ss=LINE["S",pscat];
    accept1=accept[[,4,1]];
    accept2=accept[[,4,2]];
    g=OpticsPlot[{
      {"BX","BY"},{"EX","EY"},
      {
        {Thread[{ss,accept1}],PlotJoined->Step,PlotColor->"blue",Dashing->{1}},
        {Thread[{ss,accept2}],PlotJoined->Step,PlotColor->"blue",Dashing->{1}},
        FrameLabel->"Acceptance"
      }},
      ListCoordinate->"S",Region->{from,to},Names->nm];
    Update[]

    ];

  Pc2Pp[xx_,px_,yy_,py_,dd_,ploss_]:=Module[{brho,xang,yang,bz},
    brho=LINE["GAMMABETA","$$$"]*MASS*SpeedOfLight;
    bz=LINE["BZ",ploss];
    xang=(px+yy*bz/2/brho)/(1+dd);
    yang=(py-xx*bz/2/brho)/(1+dd);
    Return[{xang,yang}];
    ];

  ReadLostParticles[]:=Module[{data,p},
    data=Get[file[1,-4]//"plost"];
    p=Flatten[Position[data,{}]];
    Scan[(data=Delete[data,#])&,Reverse[p]];
    lostParticles=Flatten[data,1];
    ];

  dumpLoss[]:=Module[{outfile,f,rsum,p1,p2,l1,l2,n0,r,x2,px2,y2,py2,dd2,nt,s1,s2},
    debugData={};
    ReadLostParticles[];

    outfile=file[1,-4]//"tab";
    f=OpenWrite[outfile];
    pz={};
    rsum=0;
    Do[
      p1=lostParticles[[i,1]];
      p2=lostParticles[[i,2]];
      l1=lscat[[Position[pscat,p1][[1,1]]]];
      If[Position[pobs,p2]=={},Print[i," ",p2]];
      l2=lobs[[Position[pobs,p2][[1,1]]]];
      If[beamgas,
        n0=ng[pvac[[Position[pscat,p1][[1,1]]]]];
        ,
        n0=PBUNCH;
        ];

!      r=PBUNCH*SpeedOfLight*n0*lostParticles[[i,3]]*l1/lsum;
      r=PBUNCH*SpeedOfLight*n0*lostParticles[[i,3]];
!      If[p2==3938,
!        Print["p1 =",p1," l1=",l1," r =",r];
!        AppendTo[debugData,{p1,l1,r}];
!        ];

      rsum=rsum+r;

      s1=LINE["S",p1]-LINE["S","IP.1"];
      s2=LINE["S",p2]-LINE["S","IP.1"];

      nt=lostParticles[[i,4]];
      x2=lostParticles[[i,11]];
      px2=lostParticles[[i,12]];
      y2=lostParticles[[i,13]];
      py2=lostParticles[[i,14]];
      dd2=lostParticles[[i,16]];
      {xangle,yangle}=Pc2Pp[x2,px2,y2,py2,dd2,p2];

      AppendTo[pz,(1+dd2)*MOMENTUM/Sqrt[Tan[xangle]^2+Tan[yangle]^2+1]];

!      Write[f,s1," ",s2," ",x2," ",xangle," ",y2," ",yangle," ",dd2," ",r," ",nt," ",l2," ",l1/lsum];
      Write[f,s1," ",s2," ",x2," ",xangle," ",y2," ",yangle," ",dd2," ",r," ",nt," ",l2," ",l1/lsum," ",p1," ",p2];

      ,{i,1,Length[lostParticles]}];

    range={Min[lostParticles[[,2]]],Max[lostParticles[[,2]]]};
    pobs2=Select[pobs,(range[[1]]<=#<=range[[2]])&];
    Do[
      pc=Position[lostParticles[[,2]],pobs2[[i]]];
      If[pc=={},
        Print[pobs2[[i]]];
        s1=0;
        s2=LINE["S",pobs2[[i]]]-LINE["S","IP.1"];
        x2=0; xangle=0; y2=0; yangle=0; dd2=0; r=0; nt=0; l2=0; l1=0; p1=0; p2=pobs2[[i]];
        Write[f,s1," ",s2," ",x2," ",xangle," ",y2," ",yangle," ",dd2," ",r," ",nt," ",l2," ",l1/lsum," ",p1," ",p2];
        ];
      ,{i,1,Length[pobs2]}];
    Close[f];
    Print["Output :",outfile," / sum of loss =",rsum];
    ];

  ];
(* End of BeamLoss Class *)

opa[from_:"^^^",to_:"$$$",nm_:"QD1*|IP.1|*MID|INJECTIO",sc_:Log]:=Module[{ss,dat,ymax,ymin,g},

  lossc=coulo@loss;
  lossb=brems@loss;
  losst=tousc@loss;

  s1=LINE["S",lossc[[,1]]];
  s2=LINE["S",lossb[[,1]]];
  s3=LINE["S",losst[[,1]]];

  ppsc=lossc[[,2]]*PBUNCH/(coulo@lobs);
  ppsb=lossb[[,2]]*PBUNCH/(brems@lobs);
  ppst=losst[[,2]]*PBUNCH/(tousc@lobs);

  ymax1=Max[ppsc]*10;
  ymax2=Max[ppsb]*10;
  ymax3=Max[ppst]*10;

  g=OpticsPlot[{
    {"BX","BY"},{"EX","EY"},
    {Thread[{s1,ppsc}],PlotJoined->Step,StepRatio->0,Scale->sc,PlotRange->{1,ymax1},
      FrameLabel->"PPS/m",Legend->"Coulomb"},
    {Thread[{s2,ppsb}],PlotJoined->Step,StepRatio->0,Scale->sc,PlotRange->{1,ymax2},
      FrameLabel->"PPS/m",Legend->"Brems"},
    {Thread[{s3,ppst}],PlotJoined->Step,StepRatio->0,Scale->sc,PlotRange->{1,ymax3},
      FrameLabel->"PPS/m",Legend->"Touschek"}
    },
    ListCoordinate->"S",Region->{from,to},Names->nm,FrameHeight->{0.2,0.2,0.24,0.24,0.24}];
  Update[]
  ];


opa2[from_:"^^^",to_:"$$$",nm_:"QD1*|IP.1|*MID|INJECTIO",norm_:1,sc_:Log]:=Module[{ss,dat,ymax,ymin,g},

  lossc=coulo@loss;
  lossb=brems@loss;
  losst=tousc@loss;

  s1=LINE["S",lossc[[,1]]];
  s2=LINE["S",lossb[[,1]]];
  s3=LINE["S",losst[[,1]]];
  If[norm==1,
    ppsc=lossc[[,2]]*PBUNCH/(coulo@lobs);
    ppst=losst[[,2]]*PBUNCH/(tousc@lobs);
    ppsb=lossb[[,2]]*PBUNCH/(brems@lobs);
    ytitle="PPS/m";
    ,
    ppsc=lossc[[,2]]*PBUNCH;
    ppst=losst[[,2]]*PBUNCH;
    ppsb=lossb[[,2]]*PBUNCH;
    ytitle="PPS";
    ];
(*
  p=Reverse[Flatten[Position[tousc@loss[[,1]],#]&/@LINE["POSITION","PMD*"]]];
  Do[
    ppst[[p[[i]]+1]]=ppst[[p[[i]]+1]]+ppst[[p[[i]]]];
    ppst=Delete[ppst,p[[i]]];
    ,{i,1,Length[p]}];
*)
  pps1=ppst;
  pps2=ppst+ppsb;
  pps3=ppst+ppsb+ppsc;

  ymax=Max[pps3]*10;
  g=OpticsPlot[{
    {"BX","BY"},{"EX","EY"},
    {
      {Thread[{s1,pps1}],PlotJoined->Step,StepRatio->0},
      {Thread[{s1,pps2}],PlotJoined->Step,StepRatio->0,Dashing->{1}},
      {Thread[{s1,pps3}],PlotJoined->Step,StepRatio->0,Dashing->{1},PlotColor->"darkgreen"},
      Scale->sc,PlotRange->{1,ymax},FrameLabel->ytitle,Legend->{"Touschek","Touschek+Brems","Touschek+Brems+Coulomb"}
      }
    },
    ListCoordinate->"S",Region->{from,to},Names->nm,FrameHeight->{0.2,0.2,0.72}];
  Update[]
  ];



WriteData[rawdata_]:=Module[{sp,v,s1,s2,x2,px2,y2,py2,dd2,xangle,yangle,fac=1},
  sp=StringPosition[infile,{"/","."}];
  outfile=infile[sp[[1,1]]+1,sp[[2,1]]-1];

  outfile=outfile//".tab";
  f=OpenWrite[outfile];
  lsum=LINE["S","$$$"]-(Twiss["DZ","$$$"]-Twiss["DZ","^^^"]);
  rsum=0;
  Do[
    v=rawdata[[i]];
    beam1=v[[1]];
    beam2=v[[2]];
    r=v[[3]];
    nt=v[[4]];

    p1=beam1[[1]];
    p2=beam2[[1]];

    s1=LINE["S",p1]-LINE["S","IP.1"];
    s2=LINE["S",p2]-LINE["S","IP.1"];

    l1=lscat[[Position[pscat,p1][[1,1]]]];
    l2=lobs[[Position[pobs,p2][[1,1]]]];

    r=fac*PBUNCH*SpeedOfLight*n0*r*l1/lsum;
    rsum=rsum+r;

    x2=beam2[[2,1,1]];
    px2=beam2[[2,2,1]];
    y2=beam2[[2,3,1]];
    py2=beam2[[2,4,1]];
    dd2=beam2[[2,6,1]];
    {xangle,yangle}=Pc2Pp[x2,px2,y2,py2,dd2,beam2[[1]]];

    Write[f,s1," ",s2," ",x2," ",xangle," ",y2," ",yangle," ",dd2," ",r," ",nt," ",l2];
    ,{i,1,Length[rawdata]}];

  Close[f];
  Print["Output file: ",outfile];
  ];
