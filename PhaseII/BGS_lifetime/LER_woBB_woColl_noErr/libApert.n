! 
!
! first by Onishi-san
! Library for apertrue set-up
!
MakeBeamLine[]:=Module[{bl,p,ap},
  bl=ExtractBeamLine[];
  p=LINE["POSITION","INJECTIO"];
  bl$inj=Join[Take[bl,{p,-1}],Take[bl,{1,p-1}]];

  FFS["USE bl$inj"];
  FFS["CELL"];
  FFS["CALC"];

  ap=Select[LINE["POSITION"],(LINE["L",#]>0)&];
  ap=Select[ap,(LINE["TYPE",#]==2||LINE["TYPE",#]==4||LINE["TYPE",#]==6||LINE["TYPE",#]==22)&];
  ap=Reverse[ap];

  SetElement["AP","APERT"];
  SetElement["APIR","APERT"];
  napir=Length[Select[LINE["NAME","QC*|EC*"],(LINE["L",#]>0)&]];
  n=0;
  Do[
    If[(LINE["NAME",ap[[i]]])[1,2]=="EC" || (LINE["NAME",ap[[i]]])[1,2]=="QC",
      n=n+1;
      If[n==napir,Continue[]];
      bl$inj=Join[Take[bl$inj,{1,ap[[i]]-1}],BeamLine[APIR],Take[bl$inj,{ap[[i]]}],Take[bl$inj,{ap[[i]]+1,-1}]];

      ,
      bl$inj=Join[Take[bl$inj,{1,ap[[i]]-1}],BeamLine[AP],Take[bl$inj,{ap[[i]]}],BeamLine[AP],Take[bl$inj,{ap[[i]]+1,-1}]];
      ];
    ,{i,1,Length[ap]}];
  AppendTo[bl$inj,BeamLine[AP]];

  mp=(Position[bl$inj,ToExpression[#]][[1,1]])&/@LINE["NAME","PMD*"];
  mp=Reverse[mp];

  SetElement["FMASK","MAP"];

  Do[
    bl$inj=Join[Take[bl$inj,{1,mp[[i]]}],BeamLine[FMASK],Take[bl$inj,{mp[[i]]+1,-1}]];
    ,{i,1,Length[mp]}];

  FFS["USE bl$inj"];
  FFS["CALC"];
  ap
  ];

Taper[s_,s1_,s2_,r1_,r2_]:=(
  r1+(r2-r1)/(s2-s1)*(s-s1)
  );

SetIRApertLER[]:=Module[{},
  comp=LINE["POSITION","APIR.*"];
  ss=0.5*(LINE["S",comp]+LINE["S",comp+1])-LINE["S","IP"];
  Do[
    If[ss[[i]]<0,
      (* R-seide *)
      day=0.001;
      sn=Abs[ss[[i]]];
      If[0.000<=sn<=0.11217,rmax1=Taper[sn,0.0,0.11217,0.00569,0.00524];rmax2=rmax1;day=0];
      If[0.11217<=sn<=0.20334,rmax1=Taper[sn,0.11217,0.20334,0.00524,0.00496];rmax2=rmax1;day=0];
      If[0.20334<=sn<=0.22000,rmax1=Taper[sn,0.20334,0.22000,0.00496,0.00500];rmax2=rmax1;day=0];
      If[0.22000<=sn<=0.45200,rmax1=Taper[sn,0.22000,0.45200,0.00500,0.01000];rmax2=rmax1;
        day=Taper[sn,0.22000,0.45200,0.0,0.001]];
      If[0.452<sn<=0.6205,rmax2=Taper[sn,0.452,0.6205,0.010,0.0135];rmax1=Taper[sn,0.452,0.6205,0.010,0.0105];day=0.001];

      If[0.6205<sn<=1.092,rmax2=0.0135;rmax1=0.0105;day=0.001];
      If[1.092<sn<=1.172,rmax2=Taper[sn,1.092,1.172,0.0135,0.0140];rmax1=Taper[sn,1.092,1.172,0.0105,0.0140];day=0.001];
      If[1.172<sn<=1.252,rmax1=Taper[sn,1.172,1.252,0.0140,0.0220];rmax2=rmax1;day=0.001];
      If[1.252<sn<=1.590,rmax1=0.0220;rmax2=rmax1;day=0.001];
      If[1.590<sn<=1.630,rmax1=Taper[sn,1.590,1.630,0.0220,0.0230];rmax2=rmax1;day=0.001];
      If[1.630<sn<=1.750,rmax1=Taper[sn,1.630,1.750,0.0230,0.0350];rmax2=rmax1;day=0.001];
      If[1.750<sn<=2.675,rmax1=0.0350;rmax2=rmax1;day=0.001];
      If[2.675<sn<=2.725,rmax1=Taper[sn,2.675,2.725,0.0350,0.0400];rmax2=rmax1;day=0.001];
      If[2.725<sn<=4.0001,rmax1=0.0400;rmax2=rmax1;day=0.001];
      ,
      (* L-seide *)
      day=0.0015;
      sp=ss[[i]];
      If[0.000<=sp<=0.095,rmax1=Taper[sp,0.0,0.095,0.00569,0.00607];rmax2=rmax1;day=0];
      If[0.095<sp<=0.245,rmax1=Taper[sp,0.095,0.245,0.00607,0.010];rmax2=rmax1;day=0];
    
      If[0.2450<sp<=0.452,day=Taper[sp,0.2450,0.452,0.0,0.0015]];
      If[0.452<sp<=0.6205,day=0.0015];

      If[0.2450<sp<=0.6205,rmax2=Taper[sp,0.2450,0.6205,0.010,0.0135];rmax1=Taper[sp,0.2450,0.6205,0.010,0.0105]];
      If[0.6205<sp<=1.092,rmax2=0.0135;rmax1=0.0105;day=0.0015];
      If[1.092<sp<=1.172,rmax2=Taper[sp,1.092,1.172,0.0135,0.014];rmax1=Taper[sp,1.092,1.172,0.0105,0.014];day=0.0015];

      If[1.172<sp<=1.252,rmax1=Taper[sp,1.172,1.252,0.014,0.022];rmax2=rmax1;day=0.0015];
      If[1.252<sp<=1.590,rmax1=0.022;rmax2=rmax1;day=0.0015];
      If[1.590<sp<=1.630,rmax1=Taper[sp,1.590,1.630,0.022,0.023];rmax2=rmax1;day=0.0015];
      If[1.630<sp<=1.750,rmax1=Taper[sp,1.630,1.750,0.023,0.035];rmax2=rmax1;day=0.0015];
      If[1.750<sp<=2.350,rmax1=0.035;rmax2=rmax1;day=0.0015];
      If[2.350<sp<=2.400,rmax1=Taper[sp,2.350,2.400,0.035,0.040];rmax2=rmax1;day=0.0015];
      If[2.400<sp<=4.0001,rmax1=0.040;rmax2=rmax1;day=0.0015];
      ];
    LINE["AX",comp[[i]]]=rmax1;
    LINE["AY",comp[[i]]]=rmax2;
    LINE["DY",comp[[i]]]=day;
!    Print[rmax1," ",rmax2," ",day];
    ,{i,1,Length[comp]}];
  comp
  ];

SetIRApertHER[]:=Module[{},
  comp=LINE["POSITION","APIR.*"];
  ss=0.5*(LINE["S",comp]+LINE["S",comp+1])-LINE["S","IP"];
  Do[
    If[ss[[i]]>0,
      sp=ss[[i]];
      dax=-7e-4;
      If[0.000 <=sp<=0.10997, rmax1=Taper[sp,0,        0.10997,0.00535,0.00544];rmax2=rmax1;dax=0];
      If[0.10997<sp<=0.270,   rmax1=Taper[sp,0.10997,  0.270,  0.00544,0.010  ];rmax2=rmax1;dax=0];
      If[0.270  <sp<=0.6205,  rmax1=Taper[sp,0.270,    0.6205, 0.0100, 0.0135 ];rmax2=rmax1;dax=0];
      If[0.6205 <sp<=1.160,   rmax1=0.0135;rmax2=rmax1;dax=0];
      If[1.160  <sp<=1.225,   rmax1=Taper[sp,1.160,    1.225,  0.0135, 0.017  ];rmax2=rmax1;dax=Taper[sp,1.160,1.225,0,-7e-4]];
      If[1.225  <sp<=1.631,   rmax1=0.017;rmax2=rmax1];
      If[1.631  <sp<=1.811,   rmax1=Taper[sp,1.631,    1.811,  0.017,  0.035  ];rmax2=rmax1];
      If[1.811  <sp<=2.675,   rmax1=0.035;rmax2=rmax1];
      If[2.675  <sp<=2.725,   rmax1=Taper[sp,2.675,    2.725,  0.035,  0.040  ];rmax2=rmax1];
      If[2.725  <sp<=4.0001,  rmax1=0.040;rmax2=rmax1];
      ,
      sn=Abs[ss[[i]]];
      dax=7e-4;
      If[0.000 <=sn<=0.10279, rmax1=Taper[sn,0,      0.10279,0.00535,0.00526];rmax2=rmax1;dax=0];
      If[0.10279<sn<=0.19598, rmax1=Taper[sn,0.10279,0.19598,0.00526,0.00497];rmax2=rmax1;dax=0];
      If[0.19598<sn<=0.210  , rmax1=Taper[sn,0.19598,0.210,  0.00497,0.00500];rmax2=rmax1;dax=0];
      If[0.210  <sn<=0.442,   rmax1=Taper[sn,0.210,  0.442,  0.00500,0.01000];rmax2=rmax1;dax=0];
      If[0.442  <sn<=0.6205,  rmax1=Taper[sn,0.442,  0.6205, 0.01000,0.0135 ];rmax2=rmax1;dax=0];
      If[0.6205 <sn<=1.160,   rmax1=0.0135;rmax2=rmax1;dax=0];
      If[1.160  <sn<=1.252,   rmax1=Taper[sn,1.160,  1.252,  0.0135, 0.0170 ];rmax2=rmax1;dax=Taper[sn,1.160,1.252,0,7e-4]];
      If[1.252  <sn<=1.631,   rmax1=0.0170;rmax2=rmax1];
      If[1.631  <sn<=1.811,   rmax1=Taper[sn,1.631,  1.811,  0.0170, 0.0350 ];rmax2=rmax1];
      If[1.811  <sn<=2.350,   rmax1=0.0350;rmax2=rmax1];
      If[2.350  <sn<=2.400,   rmax1=Taper[sn,2.350,  2.400,  0.0350, 0.0400 ];rmax2=rmax1];
      If[2.400  <sn<=4.0001,  rmax1=0.0400;rmax2=rmax1];
      ];
    LINE["AX",comp[[i]]]=rmax1;
    LINE["AY",comp[[i]]]=rmax2;
    LINE["DX",comp[[i]]]=dax;
    ,{i,1,Length[comp]}];
  comp
  ];


PlotIRApert[comp_]:=Module[{},
  gx=ListPlot[Thread[{LINE["S",comp]-LINE["S","IP"],LINE["AX",comp]}],Prolog->{
    Rectangle[{LINE["S","PQC2RO"]-LINE["S","IP"],0},{LINE["S","PQC2RI"]-LINE["S","IP"],0.045}],
    Rectangle[{LINE["S","PQC1RO"]-LINE["S","IP"],0},{LINE["S","PQC1RI"]-LINE["S","IP"],0.045}],
    Rectangle[{LINE["S","PQC1LO"]-LINE["S","IP"],0},{LINE["S","PQC1LI"]-LINE["S","IP"],0.045}],
    Rectangle[{LINE["S","PQC2LO"]-LINE["S","IP"],0},{LINE["S","PQC2LI"]-LINE["S","IP"],0.045}]
    },PointColor->"blue",PointSize->0.5];
  gy=ListPlot[Thread[{LINE["S",comp]-LINE["S","IP"],LINE["AY",comp]}],PointColor->"red",PointSize->0.5];

  gdx=ListPlot[Thread[{LINE["S",comp]-LINE["S","IP"],LINE["DX",comp]}],PlotColor->"blue",Plot->False,PlotJoined->True];
  gdy=ListPlot[Thread[{LINE["S",comp]-LINE["S","IP"],LINE["DY",comp]}],PlotColor->"red",Plot->False,PlotJoined->True];

  gox=ListPlot[Thread[{LINE["S",comp]-LINE["S","IP"],Twiss["DX",comp]}],PlotColor->"cyan",Plot->False,PlotJoined->True];
  goy=ListPlot[Thread[{LINE["S",comp]-LINE["S","IP"],Twiss["DY",comp]}],PlotColor->"magenta",Plot->False,PlotJoined->True];

  Show[Graphics[{
    Rectangle[{0,0.4},{1,1.2},gy,gx,PlotRange->{{Automatic,Automatic},{0,0.05}},FrameLabel->{"","AX,AY (m)"}],
    Rectangle[{0,0.2},{1,0.4},gdx,gox,PlotRange->{{Automatic,Automatic},{Automatic,Automatic}},FrameLabel->{"","DX (m)"}],
    Rectangle[{0,0.0},{1,0.2},gdy,goy,PlotRange->{{Automatic,Automatic},{Automatic,Automatic}},FrameLabel->{"s (m)","DY (m)"}]
    }]];

  Update[];
  ];

MachineError[]:=Module[{sd,dy},
  SeedRandom[17];
  sd=LINE["NAME","SD*"];
  dy=5e-5*GaussRandom[Length[sd]];
  Scan[(LINE["DY",#[[1]]]=#[[2]])&,Thread[{sd,dy}]];
  FFS["CALC"];
  FFS["SAVE ALL"];
  ];


! set the aperture of collimator distribution aroung MLs, values from machine tuning
! setting based on July 10-16 operation (H. Nakayama) 
 SetCollApert[whichring_:her?ler]:=
  Module[{},
    If[whichring=="HER"||whichring=="her",
      !Print["\t>> ", whichring];
      LINE[AY, "PMD01V1"] = 2.05e-3;
      LINE[DY, "PMD01V1"] = 0.05e-3;      
      LINE[AY, "PMD09V1"] = 2.5e-3;
      LINE[AY, "PMD09V2"] = 3e-3;
      LINE[AY, "PMD09V3"] = 2.5e-3;
      LINE[AY, "PMD09V4"] = 3.5e-3;
      LINE[AY, "PMD12V1"] = 4.5e-3;
      LINE[AY, "PMD12V2"] = 3.e-3;
      LINE[AY, "PMD12V3"] = 4.5e-3;
      LINE[AY, "PMD12V4"] = 4.e-3;
      LINE[AX, "PMD09H1"] = 10e-3;
      LINE[AX, "PMD09H2"] = 11e-3;
      LINE[AX, "PMD09H3"] = 13e-3;
      LINE[AX, "PMD09H4"] = 13e-3;
      LINE[AX, "PMD12H1"] = 12.5e-3;
      LINE[AX, "PMD12H2"] = 12.5e-3;
      LINE[AX, "PMD12H3"] = 13e-3;
      LINE[AX, "PMD12H4"] = 15e-3;
      LINE[AX, "PMD01H4"] = 18e-3;
      LINE[AX, "PMD01H5"] = 9.5e-3;                  
      ,
      If[whichring=="LER"||whichring=="ler",
        !Print["\t>> ", whichring];
        LINE[AY, "PMD02V1"] = 2.9e-3;
        LINE[DY, "PMD02V1"] = -0.4e-3;
        LINE[AX, "PMD06H3"] = 14e-3;
        LINE[AX, "PMD06H4"] = 12.5e-3;
        LINE[AX, "PMD02H3"] = 19.5e-3;
        LINE[AX, "PMD02H4"] = 9.5e-3;        
        ,Print["\t>> ", "Please set ring ID (her or ler).\n"]        
        ]
      ]
    ]
