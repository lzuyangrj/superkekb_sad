FFS;
Get["/ldata/SuperKEKB/SAD/Renumber.n"];
Get["/ldata/SuperKEKB/SAD/Backleg.n"];

FFS$NumericalDerivative=True;

CONVERGENCE=1e-30;

CoreService=Class[{},
  {},
  {},
  ];

(* Template of Main Ring *)
SuperKEKB=Class[{CoreService},
  {},
  {
    LatticeSetup=False,
    BaseLattice="/ldata/SuperKEKB/Lattice/LER/sler_1684.sad",
    Phase1Lattice="",
    RingName="ASC",
    RingCond="CELL",
    RingGeo="GEOFIX",
    Momentum,
    GeoIP,
    GeoAnchor,
    emit,
    RNorm,RNormInv
    },

  Constructor[]:=Module[{},
    If[~LatticeSetup,
      GetMAIN[BaseLattice];
      Momentum=MOMENTUM;
      FFS["USE "//RingName];
      FFS[RingCond];
      FFS["CALC"];
      FFS[RingGeo];
      ];

    Renumber[];
    SetAnchor[GeoAnchor];
    ];

  Circ[]:=(LINE["S","$$$"]-(Twiss["DZ","$$$"]-Twiss["DZ","^^^"]));

  SetAnchor[v_]:=Module[{p,g,c},
    p=v[[1]];
    g=v[[2]];
    c=v[[3]];
    FFS["ORG "//p//" "//g[[1]]//" "//g[[2]]//" "//g[[3]]//" ("//c[[1]]//") "//c[[2]]//" "//c[[3]]];
    FFS["GEOCAL"];
    FFS["CALC"];
    FFS[RingGeo];
    ];

  SetupEmittance[]:=(FFS["COD;CODPLOT"];
    emit=Emittance[OneTurnInformation->True];
    RNorm=NormalCoordinates/.emit;
    RNormInv=SymplecticInverse[RNorm]
    );

  InitializeParticles[np_]:=Module[{
    size=(If[emit==={},SetupEmittance[]];LINE["SIZE",1]),
    es,esd,d},
    es=RNorm.size.Transpose[RNorm];
    esd=Table[es[[i,i]],{i,6}];
    cod=Twiss[{"DX","DPX","DY","DPY","DZ","DDP"},1];
    d=GaussRandom[6,np]*Sqrt[esd];
    d=RNormInv.d+cod;
    {1,Append[d,Table[1,{np}]]}
    ];

  ];

(* LER *)
SuperLER=Class[{SuperKEKB},
  {},
  {
    },

  Constructor[]:=Module[{},
    BaseLattice="/ldata/SuperKEKB/Lattice/LER/sler_1684.sad";
    RingName="ASC";

    GeoIP={.4700000001189876,-452.8533018979187,-1.8907607239317168};
    GeoAnchor={"NMID",{-6.012926e-4,453.48,0},{-90,0,0}};

    SuperKEKB`Constructor[];
    ];

  MatchTune[nux_,nuy_]:=(
    FFS["CELL"];
    FFS["CALC",6];

    FFS["FIT PQD1C.5      PQD1C.6      BX        1"];
    FFS["FIT PQD1C.5      PQD1C.6      BY        1"];
    FFS["FIT PQD1C.5      PQD1C.6      AX        1"];
    FFS["FIT PQD1C.5      PQD1C.6      AY        1"];
    
    FFS["FIT PMID AX 0 AY 0"];
    FFS["FIT KICKER1 KICKER2 NX 0.5"];
    FFS["FIT INJECTIO BX 100"];

    If[RealQ[nux]&&RealQ[nuy],
      FFS["FIT $$$ NX "//nux//" NY "//nuy];
      ];

    FFS["FREE QI* Q{DF}RP QV* QR*"];
    
    FFS["QI{2468}* MIN 0.01 MAX 0.3"];
    FFS["QI{357}* MIN -0.3 MAX -0.01"];

    FFS["QFRP MIN 0.01 MAX 0.3"];
    FFS["QDRP MIN -0.3 MAX -0.01"];

    FFS["QV{24}* MIN 0.01 MAX 0.3"];
    FFS["QV{13}* MIN -0.3 MAX -0.01"];

    FFS["QR{246}* MIN 0.01 MAX 0.3"];
    FFS["QR{135}* MIN -0.3 MAX -0.01"];


    FFS["SAVE QI* Q{DF}RP QV* QR*"];
!    FFS["GO",6];
    );

  ];

(* HER *)
SuperHER=Class[{SuperKEKB},
  {},
  {
    },

  Constructor[]:=Module[{},
    BaseLattice="/ldata/SuperKEKB/Lattice/HER/sher_5755.sad";
    RingName="ASCE";

    GeoIP={.4700000001189876,-452.8533018979187,2.864788975654116};
    GeoAnchor={"NMID",{-0.000187,452.362954,0},{-90,0,0}};

    SuperKEKB`Constructor[];
    ];


  MatchTune[nux_:45.53,nuy_:43.57]:=(
    FFS["CELL"];
    FFS["CALC",6];
    FFS["SAVE PFUJLM"];
    FFS["SAVE PFUJRM"];
    FFS["FIT PMID AX 0 AY 0 EX 0"];
    FFS["FIT BX2E EX 0 EPX 0"];
    FFS["FIT KICKER1 KICKER2 NX 0.5"];
    FFS["FIT INJECTIO BX 100"];
    FFS["FIT QX4RE BXM 30"];
    FFS["FIT PFUJLM AX @ BX @ AY @ BY @"];

    If[RealQ[nux]&&RealQ[nuy],
      FFS["FIT $$$ NX "//nux//" NY "//nuy];
      ];

    FFS["FREE QI*E|QX*E|QM*E"];
    
    FFS["QI{246}* MIN 0.003 MAX 0.300"];
    FFS["QI{357}* MIN -0.300 MAX -0.003"];
    
    FFS["QX{246}* MIN 0.003 MAX 0.320"];
    FFS["QX{357}* MIN -0.320 MAX -0.003"];

    FFS["QM{246}* MIN 0.003 MAX 0.300"];
    FFS["QM{357}* MIN -0.300 MAX -0.003"];

    FFS["SAVE QI* QX* QM*"];
!    FFS["GO",6];
    );

  ];

! ring=SuperLER[];
