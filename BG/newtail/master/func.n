!======================================================
! Functions for tail/halo & lifetime calculation 
! by Renjun Yang
!======================================================

! calculate how much time used time0 = Date[]
T1[time0_, time1_]:= (((time1[[3]]*24+time1[[4]])*60+time1[[5]])*60+time1[[6]])-(((time0[[3]]*24+time0[[4]])*60+time0[[5]])*60+time0[[6]]); 

! extra mathematical functions
MathX=Class[{}, {}, {},
  Mean[list_]:=  Module[{res},
    res = Plus@@list/Length[list]
    ];

  StandardDeviation[list_]:= Module[{mean},
    mean = Plus@@list/Length[list];
    Sqrt[Plus@@((list-mean)^2)/Length[list]]
    ];

  (* random an integer between min. and max.*)
  RandomInteger[min_,max_]:= Module[{num,out},
    num = (max-min)*Random[]+min;
    out = If[num<Floor[num]+0.5, Floor[num], Ceiling[num]]
    ];
  ];
  
FractionPart[x_]:= x-Floor[x];
! FractionPart[{x__}]:= #-Floor[#]&/@x;

! modified from Oide-san's script
FitDistribution=Class[{},{},{},
 FitGaussian[d_,opt___]:=Module[{data=CreateBin[d],a,b,x,f,g1,g2},
   f={a/Sqrt[2],b,ConfidenceInterval,ChiSquare}/.Fit[data,a InvErf[x]+b,x,
     {a,(data[[-1,2]]-data[[1,2]])/4},{b,(data[[1,2]]+data[[-1,2]])/2},opt];
   If[Plot/.opt,
     g1=Plot[InvErf[x] f[[1]] Sqrt[2]+f[[2]],{x,-0.9999,0.9999},DisplayFunction->Identity];
     g2=ListPlot[data,DisplayFunction->Identity];
     Append[f,Show[g1,g2,opt]],
     f,
     f]];
     
 FitParabola[d_]:=Module[{data = CreatBin[d], f, a, b, c},
   f=PolynomialFit[data,2];
   a = f[1,3];   
   b = -f[1,2]/f[1,3]/2;
   c = f[1,1]-b^2;
   Thread[{a, b, c, Residual/.f[2]}]
   ];
   
 CreateBin[x_]:=Module[{xs=Sort[x],n=Length[x],x0,x1,nb,xp,lxp,ym,yms,sn=0,xm},
   nb=Floor[Sqrt[n]];
   {x0,x1}=xs[[{1,-1}]];
   xp=Partition[xs,nb];
   ym=Plus@@[xp,{1}]/(lxp=Length/@xp);
   yms=Sqrt[Plus@@[(xp-ym)^2,{1}]]/lxp;
   xm=(sn+=#&)/@lxp;
   xm=(2*(xm-xm[[1]]/2)/sn-1);
   Thread[{xm,ym,yms}]
   ];

 FitJ[d_, cut_]:=Module[{data=CreateBinJ[d, cut], f},
   f=Fit[data, 1-Exp[-x/a]+b, x, {a, (data[[-1,1]]-data[[1,1]])/4}, {b,0.}];
   {a,b,ConfidenceInterval,ChiSquare}/.f];
 FitJ[d_]:=FitJ[d, 0.];
 
 !output: CDF and coordinate of bin
 CreateBinJ[x_, cut_]:=Module[{xs=Sort[x],n=Length[x],x0,x1,nb,xp,lxp,ym,yms,sn=0,xm},
   n=Round[n*(1-cut)];
   xs=xs[#]&/@Range[n];
   nb=Floor[Sqrt[n]];
   {x0,x1}=xs[[{1,-1}]];   
   xp=Partition[xs,nb];
   ym=Plus@@[xp,{1}]/(lxp=Length/@xp);
   yms=Sqrt[Plus@@[(xp-ym)^2,{1}]]/lxp;
   xm=(sn+=#&)/@lxp;
   xm=(xm-xm[[1]]/2)/sn;
   Thread[{ym, xm}]
   ];
   
 CreateBinJ[x_]:=CreateBinJ[x,0.];
 
 InvErf[x_Real]:=Module[{y},
   y/.FindRoot[Erf[y]==x,{y,0,{-Infinity,Infinity}}]];
 ];

! require "beamline.n" and "libApert.n" lib
With[{default={IR->False, Coll->False, Pipe->False}},
  SetAperture[ring_, ldir_, opt___]:=
    Module[{},
      op = Override[opt, default];
      {aptIR, aptColl, aptp} = {IR, Coll, Pipe}/.op;
      Get[ldir//"beamline.n"];
      Get[ldir//"libApert.n"];  
      MakeBeamLine[];
      If[aptIR, SetIRApertLER[]];
      If[aptColl, SetCollApert[ring]];
      If[aptp,
        Scan[(LINE["AX",#]=0.052;LINE["AY",#]=0.025;LINE["DX",#]=Twiss["DX",#];LINE["DY",#]=Twiss["DY",#];)&, LINE["NAME","AP.*"]];
        Scan[(LINE["AX",#]=0.040;LINE["AY",#]=0.040;LINE["DX",#]=Twiss["DX",#];LINE["DY",#]=Twiss["DY",#];)&, Select[LINE["NAME","AP.*"],(LINE["POSITION","ZVQLA5LE"]<LINE["POSITION",#]<LINE["POSITION","ZVQLA5RE"])&]]
        ];
      Print["\t>> IR Apert="//aptIR//"\t Coll Apert="//aptColl//"Pipe ="//aptp//"\n"];
      ];
    ];

With[{def={RandomNumber->True}},
  MachineError[erry_:m, opt___]:=Module[{sd, dy, op, r, nrand = {.617159,1.657394,-.476296,.422829,-1.399247,.775955,-.529322,-.261079,-2.360512,.941392,-.061057, .511991,.568802,.336598,-2.023014,-1.851276,-.975401,.021664,-.054168,-.714473,-.940776,-.34548, .425474,.608199,-.288556,-1.241462,-.191341,-.03309,-.053497,.410508,-.056906,.626442,.70292, 1.554713,-1.929324,-.366252,-.170697,.062924,-.050859,.605321,.80067,.735858,-.181247,-3.440788, .633706,.322126,-.043232,1.535166,-.912981,.035196,-1.062549,.297823,.05434,.188666,.328514, -.090524,.899766,-.482603,-.308941,-1.264601,-1.570614,.775896,.310993,-1.909206,.252068,-1.069879, .926976,-.094778,-.741787,-.886926,.400714,.372849,-.865045,-.922646,.863671,.947675,2.35462, .881997,1.104272,-.191543,.116413,-.598103,-1.033002,-.722158,-2.147041,.419295,-.25479,-1.188472, .030489,.418397,1.051369,-1.609735,-.040889,-1.577976,.458448,1.325502,.071669,-.188751,1.088422, .364316}},
    op = Override[opt, def];
    r = RandomNumber/.op;
    sd=LINE["NAME","SD*"];
    dy = If[r,
            erry*GaussRandom[Length[sd]]
           ,erry*nrand[Range[Length@sd]]];
    Scan[(LINE["DY",#[[1]]]=#[[2]])&,Thread[{sd,dy}]];
    FFS["CAL NOEXP"];
    !FFS["SAVE ALL"];
    ];
  ];

ExportTwiss[fname_]:=
  Module[{wholeEles0, wholeElesPos0, totalElesNum, listEles, posEles, numEles, twiss0, deltai, i, j, fw, tmp=0},
    wholeEles0 = LINE["NAME", "*"];
    wholeElesPos0 = LINE["S", "*"];
    totalElesNum = Length[wholeEles0];
    totalLength0 = LINE["S",wholeEles0[-1]];
    listEles = {};
    posEles = {};
    numEles = {};
    i=1;
    While[i<totalElesNum,
         If[i==1||wholeElesPos0[i]>(tmp+1e-3),
           AppendTo[listEles, wholeEles0[i]];
           AppendTo[posEles, wholeElesPos0[i]];
           AppendTo[numEles, i];
           tmp = wholeElesPos0[i]];
         i++;
         ];
    If[i<=totalElesNum,
      twiss0=Transpose[Twiss[{"BX", "AX", 'BY', 'AY', 'EX', 'EPX', "EY", "EPY", "DX", "DPX", "DY", "DPY"}, listEles]];
      If[Length[Position[Twiss0, {}]], Print["   Errors: nulls in Twiss Matrix."]];   
      ! write optical function
      $FORM = "12.6";
      fw = OpenWrite[fname//".dat"];
      Do[
        WriteString[fw, listEles[i], "\t", posEles[i],"\t",numEles[i],"\t"];
        Do[WriteString[fw, twiss0[i,j], "\t"], {j, 1, 12}];
        WriteString[fw, "\n"]
       ,{i,Length[twiss0]}];
      Close[fw];
      $FORM = "S17.15";
      ];
    ];

ImportTwiss[fn_]:=Module[{fr, name, lev1={}, lev2},
  fr = OpenRead[fn//".dat"];
  Do[
    name = Read[fr, Word];  
    If[ToString[name]=="EndOfFile",Break[]];
    lev2 = {name};
    Scan[AppendTo[lev2, Read[fr, Real]]&, Range[14]];
    AppendTo[lev1, lev2]
    ,{100000}];
  Thread@lev1];

PrintEmit[___]:=Module[{e, emitx, emity, emits, sigp, sigz},
 e=Emittance[OneTurnInformation->True, ExpandElementValues->False];
 {emitx, emity, emits} = Emittances/.e;
 ! If[emity<emitx*MINCOUP, emity=emitx*MINCOUP];
 sigp = MomentumSpread/.e;
 sigz = BunchLength/.e;

 Print["\t>> Beam parameters:\n >> emitx= "//emitx//"\n >> emity= "//emity//"\n >> emits= "//emits//"\n >> sigp= "//sigp//"\n >> sigz= "//sigz//"\n"];
 ];

! export emitx, emiy and emis
! input: np*6 particle distribution
With[{def={append->False}},
  Export[name_, data_, opt___] := Module[{op, nl,nw},
    op = Override[opt, def]; 
    $FORM = '12.6';
    {nl, nw} = Dimensions[data];
    If[Length[Dimensions[data]]<>2, nl = Length[data]; nw = Length[data[1]]];
    System["rm "//name];
    fx = If[append/.op, OpenAppend[name], OpenWrite[name]];
    Table[
      Table[WriteString[fx,data[i,j],"\t"],{j,1,nw}];
      WriteString[fx,"\n"];
      ,{i,1,nl}];
    Close[fx];
    $FORM = "S17.15";   
    ];

  Import[fn_, ncolumn_]:=Module[{fr, fg, lev1={}, lev2},
    fr = OpenRead[fn];
    Do[
      fg = Read[fr, Word];  
      If[ToString[fg]=="EndOfFile",Break[]];
      lev2 = {fg}; 
      Scan[AppendTo[lev2, Read[fr, Real]]&, Range[ncolumn-1]];
      AppendTo[lev1, lev2]
      ,{100000}];
    lev1];
   ];

! fn: name, pos: component position
ImportBeam[fn_, pos_] := Module[{x={},x0,px={},y={},py={},z={},dp={},flag={}, fr, j},
  fr = OpenRead[fn];
  Do[
     x0 = Read[fr, Real]; (*decide the end of file*)
     If[ToString[x0]=="EndOfFile",Break[]];
     AppendTo[x, x0];
     AppendTo[px, Read[fr, Real]];
     AppendTo[y, Read[fr, Real]];
     AppendTo[py, Read[fr, Real]];
     AppendTo[z, Read[fr, Real]];
     AppendTo[dp, Read[fr, Real]];
     AppendTo[flag, Read[fr, Real]]
     ,{j, 1, 10000000}];
  Close[fr];
  If[~(Length[x]), Print["\tError when import :", fn, "\n"]];
  {pos, {x, px, y, py, z, dp, flag}}];
  
ImportBeam[fn_]:=ImportBeam[fn, 1];

AppendBeam[b1_, b2_]:=Module[{},
  If[b1[1]==b2[1], {b1[1], Thread[Join[Thread[b1[2]], Thread[b2[2]]]]}]  
  ];

! Opinions:
! Method: "Linear" or "SAD" tracking using transfer matrix or TrackParticles[]
! SelfTeminate: Stop tracking when reaching equilibrium emittances
! DataPath: "./data/" (default) path for data saving
! Method: "Static" (defult) or "Fit", method to compute emittance
! PrintOut: Print emittance evaluation per turn or not
! OneTurnPerturbation: False (default) turn-by-turn RAD/FLUC or not; for only "SAD" Method
! Damping/Excitation: turn-by-turn RAD/FLUC on/off
! XYcoup: False (default), affect way to implement RAD/FLUC per turn; for only "SAD" Method
With[{default={SelfTeminate->False, DataPath->"./data/", Method->"Static", PrintOut->False, Cut->0.0}, def2={Method->"SAD", OneTurnPerturbation->False, Damping->False, Excitation->False, XYcoup->True}},
  ! pos is integer, "$$$" or "^^^$; cut -> the tail cut
  ! generate Gaussian distributed particles at any location taking into account dispersion and xy coupling, orbit
  GenGaussBeam[pos_, np_Real, cut_Real, emit_] := Module[{e, mpn, orb0, tms, mnpp, p, orbs, i, jx2, jy2, js2, psix, psiy, psis, x, px, y, py, z, pz, flag, beam0, x6},
    e = Emittance[OneTurnInformation->True, Matrix->True, ExpandElementValues->False];
    {mpn, orb0, orbs, tms} = {NormalCoordinates, OrbitAtEntrance, ClosedOrbit, TransferMatrices}/.e;
    p = LINE["POSITION", pos];
    mnpp = If[p>1&&p<LINE["POSITION", $$$],
             tms[p].SymplecticInverse[mpn],                
             SymplecticInverse[mpn]
             ];
    FFS["GCUT="//cut];
    ! 2Jx
    {jx2, jy2, js2} = Table[-2*emit[i]*Log[1-Random[np]], {i, 3}];
    {psix, psiy, psis} = Table[2*PI*Random[np], {3}]; 
    {x, px} = {Sqrt[jx2]*Cos[psix], -Sqrt[jx2]*Sin[psix]};
    {y, py} = {Sqrt[jy2]*Cos[psiy], -Sqrt[jy2]*Sin[psiy]};
    {z, dp} = {Sqrt[js2]*Cos[psis], -Sqrt[js2]*Sin[psis]};
    flag = Table[1,{np}];    
    x6 = {x, px, y, py, z, dp};
    x6 = mnpp.x6;
    x6=x6+orbs[p];
    beam0 = {p, Append[x6, flag]};
    beam0
    ];
    
  GenGaussBeam[pos_, np_Real, cut_Real]:=GenGaussBeam[pos, np, cut, {EMITX, EMITY, EMITZ}];
  GenGaussBeam[pos_, np_Real]:=GenGaussBeam[pos, np, 3, {EMITX, EMITY, EMITZ}];  
  GenGaussBeam[pos_]:=GenGaussBeam[pos, 1, 3, {EMITX, EMITY, EMITZ}];
  
  ! remove lost particles
  SiftBeam[b0_] := Module[{alive={}, b2={}},
    ! Position[] doesn't work for the old KEK SAD.
      b2 = {b0[1], SurvivedParticles[b0[2]]};
      If[~(Length[b2[2,1]]), Print["\tAll particles lost at "//ToString[LINE["NAME", b0[1]]]//".\n"]];
      b2];

  ! evaluat emittance in two kind of methods, i.e., static and fit.
  ! EMITX, EMITY are for normal coordinates (w/o coupling, dispersion)
  CalEmit[beam0_, opt___] := Module[{op, method, math, ft, mpn, orb0, tms, mpnp, p, orbs, b2, x6, x6n, J, psi, i, emx=0, emy=0, ems=0, cut},
    op = Override[opt, default];
    method = Method/.op;
    cut = Cut/.op;
    math = MathX[];
    ft = FitDistribution[];
    If[~(Length[e$]), e$ = Emittance[OneTurnInformation->True, Matrix->True, ExpandElementValues->False]];
    {mpn, orb0, orbs, tms} = {NormalCoordinates, OrbitAtEntrance, ClosedOrbit, TransferMatrices}/.e$;
    p = beam0[1];
    mpnp = If[p>1&&p<LINE["POSITION", $$$],
             mpn.SymplecticInverse[tms[p]],
             mpn
             ];             
    b2 = SiftBeam[beam0];  ! particle survey
    x6 = Drop[b2[2], -1]-orbs[p];
    x6n = mpnp.x6;

    ! J, psi at the question position;
    J = Table[(x6n[i]^2+x6n[i+1]^2)/2, {i,1,5,2}];
    psi = Table[ArcTan[-x6n[i+1]/x6n[i]], {i,1,5,2}];
    psi = MapAt[-1*#&, psi, {3}];
    {emx, emy, ems} = If[method=="Static",      
                        Table[math@Mean[J[i]], {i,3}],
                        If[method=="Fit",
                          Table[ft@FitJ[J[i], cut][1], {i,3}]
                          ]
                        ]; 
    {{emx, emy, ems}, {J, psi}}
    ];
    
  ! default: ^^^ to $$$ until nmax-th turn
  TrackCore[nmax_, naccu_, step_, np_, opt___] := Module[{op, op2, datpath, e, em0, EMITT, emitt, tol=2e-2, b0, b2, cut},
    op = Override[opt, default];
    op2 = Override[opt, def2];
    
    datpath = DataPath/.op;  
    e = Emittance[OneTurnInformation->True, Matrix->True, ExpandElementValues->False];
    em0 = Emittances/.e;
    
    b0 = GenGaussBeam["^^^", np, cut=100];
    If[~(DirectoryQ[datpath]), CreateDirectory[datpath]];
    EMITT = {};
    Do[
      b2 = TrackParticles2[b0, "$$$", step, op2]; ! to be improved, 16 Apr 2019
      emitt = CalEmit[b0][1];
      If[PrintOut/.op, Print["\tnturn = ", i, "\temitx= ", emitt[1], "\t emity= ", emitt[2], "\t emits= ", emitt[3], "\n"]];
      b0 = {1, b2[2]};
      AppendTo[EMITT, Flatten@{i, emitt}];
      If[SelfTerminate/.op,
        If[Abs[emitt[1]-em0[1]]/em0[1]<tol && Abs[emitt[2]-em0[2]]/em0[2]<tol && Abs[emitt[3]-em0[3]]/em0[3]<tol,
          Export[datpath//"dis_"//i//".dat", Thread[b2[2]]],
          If[i>=nmax-naccu, Export[datpath//"dis_"//i//".dat", Thread[b2[2]]]]
          ];
        ]
      ,{i, 1, nmax}];        
    Export[datpath//"emit.dat", EMITT];
    ];
  ];

! quick check of beam passage
TestBeamPassage[]:= Module[{beam0, beam1},
  beam0 = GenGaussBeam["^^^", 100, 3];
  Do[
    beam1 = TrackParticles[beam0, i];
    If[~(Plus@@beam1[2,7]), Print[LINE["NAME", i]]; Break[]];
    beam0 = beam1;
    ,{i, 2, LINE["POSITION", $$$]}];
  If[beam0[1]==LINE["POSITION", $$$], Print["\tBeam pass the entire beam line.\n"]];
  ];

(* beam tracking with one-turn damping and excitation *)
! Constrains:
! 1) nturns trakcing from p1 to p2 (p1<p2 if nturn<2)
! 2) OneTurnPerturbation affect only "SAD" tracking; XYcoup only for OneTurnPerturbation->True
! 3) Method->"Linear" means tracking with one-turn transfer, damping and exicitation matrices. COUP->True; w/o aperture
With[{def = {Method->"SAD", OneTurnPerturbation->False, Damping->False, Excitation->False, XYcoup->True, IBS->False}},
  TrackParticles2[beam0_, pos_, nturn0_, opt___]:=Module[{op, pos0, pos1, nturn = 1, np, emis, mnp, mpn, orbs, xorb0, xorb1, xorb2, tms, tm, tmi, md, mex, dmpr, bex, aa, eigenValue, eigenVector, beam, beam1, pflag, x6, x0, xn, xdamp, xexcite, i, ii, beam2},
    op = Override[opt, def];
    pos0 = beam0[1];
    pos1 = LINE["POSITION", pos];
    nturn = If[RealQ[nturn0], nturn0, 1];
    If[nturn==1, If[beam0[1]>pos1, Print["\tEnd-point should be larger than start-point (one-turn tracking)!"]]];
    np = Length[beam0[2,7]];
    
    If[~(Length@e$), e$=Emittance[OneTurnInformation->True, Matrix->True, ExpandElementValues->False]];
    {emis, mnp, orbs, tms, md, mex} = {Emittances, NormalCoordinates, ClosedOrbit, TransferMatrices, OneTurnDampingMatrix, OneTurnExcitation}/.e$;
    mpn = SymplecticInverse[mnp];
    dmpr = -DampingRate/.e$;
    bex = Sqrt[2.0*emis*dmpr];
    xorb0 = orbs[pos0]; ! initial
    xorb1 = orbs[pos1]; ! destination
    xorb2 = orbs[-1]; ! at $$$
    aa = Eigensystem[mex];
    {eigenValue, eigenVector} = aa[{1,2}];

    If[(Method/.op)=="SAD",
      If[OneTurnPerturbation/.op,
        FFS["NORAD; NOFLUC;"];
        beam = beam0;
        Do[
          beam1 = TrackParticles[beam, "$$$"];
          pflag = beam1[2, 7];
          x0 = Drop[beam[2], -1];          
          x6 = Drop[beam1[2], -1];
          x6 -= xorb2;
          x0 -= xorb2;
          If[XYcoup/.op,
            xdamp = If[Damping/.op, md.x0, md.(x0-x0)];                               
            If[Excitation/.op,
               xexcite = Table[GaussRandom[np],{6}];
               xexcite = Transpose[eigenVector].(Sqrt[eigenValue]*xexcite),
               xexcite = Table[Table[0, {np}], {6}];
              ];
            x6 += (xdamp+xexcite);
            ,
            xn = mnp.x6;
            If[~(Damping/.op), dmpr = {0, 0, 0}];
            If[~(Excitation/.op), bex = {0, 0, 0}];          
            xn[[1]] = xn[[1]]*(1.-dmpr[[1]]) + bex[[1]]*GaussRandom[np];
            xn[[2]] = xn[[2]]*(1.-dmpr[[1]]) + bex[[1]]*GaussRandom[np];
            xn[[3]] = xn[[3]]*(1.-dmpr[[2]]) + bex[[2]]*GaussRandom[np];
            xn[[4]] = xn[[4]]*(1.-dmpr[[2]]) + bex[[2]]*GaussRandom[np];
            xn[[5]] = xn[[5]]*(1.-dmpr[[3]]) + bex[[3]]*GaussRandom[np];
            xn[[6]] = xn[[6]]*(1.-dmpr[[3]]) + bex[[3]]*GaussRandom[np];              
            x6 = mpn.xn;            
            ];            
          x6 += xorb2;
          beam ={1, Append[x6,pflag]};
          ,{nturn-1}];
        beam = TrackParticles[beam, pos1];
        beam2 = beam, ! If[OneTurn...]
        If[Damping/.op, FFS["RAD;"], FFS["NORAD;"]];
        If[Excitation/.op, FFS["FLUC;"], FFS["NOFLUC;"]];
        beam2 = TrackParticles[beam0, pos1,,nturn];
      ]];
       
    If[(Method/.op)=="Linear",
       x0 = Drop[beam0[2], -1];
       x0 -= xorb0;
       pflag = beam0[2, 7];
       ii = pos0;
       Do[
         tm = If[i<2, tms[-1].SymplecticInverse[tms[pos0]], tms[-1]];
         ! assuming beam center at the closed orbit (?, k64)
         If[IBS/.op||Wake/.op,
           If[ii<Length[obs],
             tmi = tms[ii+1].SymplecticInverse[tms[ii]];
             ii++;
             x0 = tmi.x0;
             If[IBS/.op,
               x0 = IntraBeamMap[x0, orb[ii]]];
             ],
           x6 = tm.x0,
           ];
         xdamp = If[Damping/.op, md.x0, md.(x0-x0)];                     
         If[Excitation/.op,
            xexcite = Table[GaussRandom[np],{6}];
            xexcite = Transpose[eigenVector].(Sqrt[eigenValue]*xexcite),
            xexcite = Table[Table[0, {np}], {6}];
           ];
         x6 += (xdamp+xexcite);
         x0 = x6;
         ,{i, nturn-1}];
       tm = tms[pos1].SymplecticInverse[tms[pos0]];
       x6 = tm.x0;       
       x6 += xorb1;         
       beam2 = {pos1, Append[x6, pflag]};
       ];
    !Print["\tnturn = ", nturn, "\tnp = ", np, "\tMethod -> ", Method/.op, "\n"];
    !Print["\tDamping -> ", Damping/.op, "\tExcitation -> ", Excitation/.op, "\tXYcoup -> ", XYcoup/.op, "\n"];
    beam2];

  !!! TrackParticles2[beam0, {op1, op2}];
  TrackParticles2[beam0_, opt___]:=  Module[{}, TrackParticles2[beam0, "$$$", 1, opt]];
  TrackParticles2[beam0_, pos_, opt___]:=  Module[{}, TrackParticles2[beam0, pos, 1, opt]];

  TimeTest[]:=Module[{beam0, t0, t1, t2, t3},
    beam0 = GenGaussBeam["^^^", 50, 3];
    t0 = Date[];
    TrackParticles2[beam0, "$$$", 1000, Damping->True, Excitation->True, Method->"Linear"];
    t1 = T1[Date[], t0];    
    TrackParticles2[beam0, "$$$", 1000, Damping->True, Excitation->True, Method->"SAD", OneTurnPerturbation->True];
    t2 = T1[Date[], t0]-t1;
    TrackParticles2[beam0, "$$$", 1000, Damping->True, Excitation->True, Method->"SAD", OneTurnPerturbation->False];
    t3 = T1[Date[], t0]-t1-t2;
    $FORM = '10.2';
    Print["\tLinear ", t1, "\tSAD (turn-by-turn) ", t2, "\tSAD (element-by-element) ", t3, "\n"];
    $FORM =  'S17.15';    
    {t1, t2, t3}]
  ];

! for extra particle tracking 
With[{def={Method->"SAD", OneTurnPerturbation->True, Damping->True, Excitation->True, XYcoup->True, SaveBeam->False}},
  ExtraTracking[beam0_, turn0_, nturn_, opt___] := Module[{op, beam, beam1, i},
    op = Override[opt, def];
    beam = beam0;
    Do[
      beam1 = TrackParticles2[beam, op];
      beam1 = SiftBeam[beam1];
      If[SaveBeam/.op, Export["./data/beam_turn="//ToString[turn0+i]//".dat", Thread[beam1[2]]]];
      beam = {1, beam1[2]}
      ,{i, nturn}];
    ];
  ];

Protect[TrackParticles2, Export, Import, ImportBeam, AppendBeam, MathX];