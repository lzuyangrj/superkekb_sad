!==============================================================
! elastic (CO)
!==============================================================

dsigmaCO[theta_]:=
(* calculation of relative cross section *)
Module[{sigmaCO},
        sigmaCO=Sin[theta]/(theta^2+thetam1^2)^2;
        sigmaCO
      ];

InitializemkBG1:=
(* To generate the elastic scattering perturbation *)
  Module[{hlim,llim,nbins,every,xx,xx1,xx2,xbin,areaSwap,area0,output},
        llim = 50*thetam1;
        nbins = 2e4;
        hlim = 2e5*thetam1;
        !xx=(hlim-llim)*Random[nbins-1]+llim;
        xx = Table[llim+i*(hlim-llim)/nbins,{i,1,nbins-1}];
        xx1 = Append[xx,llim];      ! insert the llim and hlim
        xx1 = Sort[xx1];             ! sort into canonical order
        xx2 = Append[xx,hlim];      ! insert the llim and hlim
        xx2 = Sort[xx2];             ! sort into canonical orde
        xbin = xx2-xx1;              ! length of every bins (xx)
        areaSwap = Table[dsigmaCO[xx1[i]]*xbin[i],{i,1,nbins}];

        ! 2016-12-19	 modify because errros with Sum[] for old SAD @ IHEP servers
        area0 = Table[Apply[Plus,Table[areaSwap[i],{i,1,j}]],{j,1,nbins}];

        areamkBG1 = area0/area0[nbins];
        xx1mkBG1 = xx1;
     ];

mkBG1[nn_]:=
  (* To generate the elastic scattering perturbation; bins should be 15000 coresponding to initialziemkBG1 function *)
  Module[{proi,every,output},
        output={};

        proi=Min@areamkBG1+(Max@areamkBG1-Min@areamkBG1)*Random[nn];
        For[i=1, i<=nn, i++,
                Do[
                  If[proi[i]<areamkBG1[j],every=xx1mkBG1[j-1];output=Append[output,every];Break[]]
                ,{j,1,2e4}];
           ];
        output
     ];

mkBG1NoKick[nn_]:=
  (* To generate the elastic scattering perturbation; bins should be 15000 coresponding to initialziemkBG1 function *)
  Module[{proi,every,output},
        output={};

        proi=Min@areamkBG1+(Max@areamkBG1-Min@areamkBG1)*Random[nn];
        For[i=1, i<=nn, i++,
                Do[
                  If[proi[i]<areamkBG1[j],every=0;output=Append[output,every];Break[]]
                ,{j,1,1.5e4}];
           ];
        output
     ];

!==============================================================
! inelastic (CO)
!==============================================================

dsigmaieCO[kk_]:=
(* calculation of relative cross section of inelastic process CO
kk = Es/E
*)
Module[{z01=6,z02=8,Fz1,Fz2,sigmaie1,sigmaie2,sigmaie},
       Fz1 = z01^2*Log[183/z01^(1/3)]+z01*Log[1194/z01^(2/3)];
       Fz2 = z02^2*Log[183/z02^(1/3)]+z02*Log[1194/z02^(2/3)];
       !sigmaie1 = Fz1*(-4/3*Log[kk]-kk^2/2+4/3*kk-5/6)+1/9*(z01^2+z01)*(-Log[kk]+kk-1);
       !sigmaie2 = Fz2*(-4/3*Log[kk]-kk^2/2+4/3*kk-5/6)+1/9*(z02^2+z02)*(-Log[kk]+kk-1);

       sigmaie1 = (4/3/kk-4/3+kk)*Fz1+1/9*(1/kk-1)*(z01^2+z01);
       sigmaie2 = (4/3/kk-4/3+kk)*Fz2+1/9*(1/kk-1)*(z02^2+z02);
       sigmaie = sigmaie1+sigmaie2
      ];

InitializemkBG2CO:=
  (* for mkBG2CO function; nbins -> 20000; Ek_min = 5e-3*Ek; Ek_max = 0.9*Ek >> initial : xx1mkBG2CO && areamkBG2CO *)
  Module[{hlim,llim,nbins,every,xx,xx1,xx2,xbin,areaSwap,area0,area,kk=1e-5},
        nbins=20000;
        llim=kk;
        hlim=0.9; ! Accep. = 1 %
        xx = Table[llim+i*(hlim-llim)/nbins,{i,1,nbins-1}];
        xx1=Append[xx,llim];      ! insert the llim and hlim
        xx1=Sort[xx1];             ! sort into canonical order
        xx2=Append[xx,hlim];      ! insert the llim and hlim
        xx2=Sort[xx2];             ! sort into canonical orde
        xbin=xx2-xx1;              ! length of every bins (xx)
        areaSwap=Table[dsigmaieCO[xx1[i]]*xbin[i],{i,1,nbins}];

        area0 = Table[Apply[Plus,Table[areaSwap[i],{i,1,j}]],{j,1,nbins}];
        !area0=Table[Sum[areaSwap[i],{i,j}],{j,1,nbins}];  ! 2016-12-19 modify because errros with Sum[] for old SAD @ IHEP servers

        area=area0/area0[nbins];
        xx1mkBG2CO = xx1;
        areamkBG2CO = area;
      ];

mkBG2CO[nn_]:=
(* To generate the elastic scattering perturbation
nn-> number of scatterings, kk -> min. ratio of Es/E which is the min. value of energy transfer between e- and atom
output -> d(dp/p0)
2016/09/06 -> correct random bins to uniform bins
2016/09/28 -> require initialization by initializemkBG2CO; nbins -> 15000; 
 *)
  Module[{proi,every,output},
        output ={};
        proi=Min@areamkBG2CO+(Max@areamkBG2CO-Min@areamkBG2CO)*Random[nn];
        For[i=1, i<=nn, i++,
                Do[
                   If[proi[i]<areamkBG2CO[j],every=xx1mkBG2CO[j-1];output=Append[output,every];Break[]]
                ,{j,1,20000}];
           ];
        output
     ];

mkBG2CONoKick[nn_]:=
(* To generate the elastic scattering perturbation
nn-> number of scatterings, kk -> min. ratio of Es/E which is the min. value of energy transfer between e- and atom
output -> d(dp/p0)
2016/09/06 -> correct random bins to uniform bins
2016/09/28 -> require initialization by initializemkBG2CO; nbins -> 15000; 
 *)
  Module[{proi,every,output},
        output ={};
        proi=Min@areamkBG2CO+(Max@areamkBG2CO-Min@areamkBG2CO)*Random[nn];
        For[i=1, i<=nn, i++,
                Do[
                   If[proi[i]<areamkBG2CO[j],every=0;output=Append[output,every];Break[]]
                ,{j,1,20000}];
           ];
        output
     ];
