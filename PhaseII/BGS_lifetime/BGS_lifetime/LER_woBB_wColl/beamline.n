
IpPoint[]:=(
  {.4700000001189876,-452.8533018979187}
  );


SetGeom[]:=(
  Which[
    Element["NAME","B*E"]<=>{},( (* HER *)
      FFS["ORG NMID -0.000187 452.362954 0 (-90) 0 0"];
!      FFS["ORG PQFRNI .539773 452.362954 0 (-90) 0 0"];
      ),
    Element["NAME","B*P"]<=>{},( (* LER *)
      FFS["ORG NMID -6.012926e-4 453.48 0 (-90) 0 0"];
      ),
    True, (* Nothing *)
    ];
  FFS["GEOCAL"];
  FFS["CALC"];
  Print["IP Position"];
  FFS["DISP G IP.1",6];
  );

ExtLine[from_,to_]:=Module[{ax0,bx0,ay0,by0,ex0,epx0,ey0,epy0},
  {ax0,bx0,ay0,by0,ex0,epx0,ey0,epy0}=Twiss[{"AX","BX","AY","BY","EX","EPX","EY","EPY"},from];

  a=ExtractBeamLine[BeamLineName[]];
  b=Take[a,{from,to}];

  If[LINE["TYPE",from]<>41,
    SetElement["PSTART","MARK"];
    b=Prepend[b,PSTART];
    ];

  FFS["USE b"];
  FFS["INS"];

  FFS["axi "//ax0//" bxi "//bx0//" ayi "//ay0//" byi "//by0];
  FFS["exi "//ex0//" epxi "//epx0//" eyi "//ey0//" epyi "//epy0];
 
  FFS["CALC"];
  ];

WriteEle[opt_:False]:=Module[{f,ds,file},
  If[opt,
    FFS["INTRA"];
    FFS["EMIT"];
    FFS["EMITY ="//(EMITX*MINCOUP)];
    emt=Emittance[];
    FFS["DP ="//(MomentumSpread/.emt)];
    FFS["NOINTRA"];
    ];
  ds=StringReplace[DateString[],{"/"->""," "->""}];
  file=ds[5,8]//"-"//ds[1,4]//"-"//ds[9,-1];

  file=file//".deck";
  f=OpenWrite[file];
  Write[f," ;"];
  Write[f," OFF CTIME;"];
  
  FFS["output "//f//" type"];
  Close[f];
  Print["Output deck file to : ",file];
  Return[file];
  ];

WriteLine["HELP"]:=Print["WriteLine[file,line,opt_False]"];

WriteLine[file_:"tmp.sad",line_:"",opt_:False]:=Module[{f},
  f=OpenWrite[file];
  If[opt,
    FFS["INTRA"];
    FFS["EMIT"];
    FFS["EMITY ="//(EMITX*MINCOUP)];
    emt=Emittance[];
    FFS["DP ="//(MomentumSpread/.emt)];

    Write[f," ;"];
    Write[f," OFF CTIME;"];
    FFS["output "//f//" type"];
    ];
  Write[f," ;"];
  Write[f," LINE ",line,"="];
  WriteBeamLine[f,ExtractBeamLine[],Format->"MAIN"];
  Write[f," ;"];

  Write[f,"MOMENTUM = "//MOMENTUM//";"];
  Write[f,"PBUNCH ="//PBUNCH//";"];
  Write[f,"MINCOUP ="//MINCOUP//";"];
  Write[f,"FSHIFT ="//FSHIFT//";"];

  Close[f];
  Print["Output deck file to : ",file," >>> BeamLine : ",line];
  ];

InsertVOC[]:=Module[{p,ap,c},
  bl=ExtractBeamLine[];
  p=LINE["POSITION","ZVQC{12}*"];
  ap={"VOC1LE","VOC2LE","VOC2RE","VOC1RE"};
  SetElement[#,"MULT"]&/@ap;
  c=Reverse[Thread[{p,ap}]];
  Do[
    bl=Join[Take[bl,c[[i,1]]],BeamLine[c[[i,2]]],Take[bl,{c[[i,1]]+1,-1}]]
    ,{i,1,Length[c]}];

  FFS["USE bl"];
  FFS["CALC"];
  ];

InsertApert[apert_:{}]:=Module[{p,ap,c},
  bl=ExtractBeamLine[];
  p=LINE["POSITION","PQC{12}*"];
  If[apert=={},
    ap={
      {"AQC1LI",0.0007,0.017,0.017},
      {"AQC1LC",0.0007,0.017,0.017},
      {"AQC1LO",0.0007,0.017,0.017},
      {"AQC2LI",0.0007,0.040,0.040},
      {"AQC2LC",0.0007,0.040,0.040},
      {"AQC2LO",0.0007,0.040,0.040},
      {"AQC2RO",-0.0007,0.040,0.040},
      {"AQC2RC",-0.0007,0.040,0.040},
      {"AQC2RI",-0.0007,0.040,0.040},
      {"AQC1RO",-0.0007,0.017,0.017},
      {"AQC1RC",-0.0007,0.017,0.017},
      {"AQC1RI",-0.0007,0.017,0.017}
      };
    ,
    ap=apert;
    ];
  SetElement[#[[1]],"APERT",{"DX"->#[[2]],"AX"->#[[3]],"AY"->#[[4]]}]&/@ap;
  c=Reverse[Thread[{p,ap[[,1]]}]];
  Do[
    bl=Join[Take[bl,c[[i,1]]],BeamLine[c[[i,2]]],Take[bl,{c[[i,1]]+1,-1}]]
    ,{i,1,Length[c]}];

  FFS["USE bl"];
  FFS["CALC"];
  ];

SetPbunch[totcur_:2.6,nb_:2500]:=(
  PBUNCH=totcur/ElectronCharge*(LINE["S","$$$"]-Twiss["DZ","$$$"])/nb/SpeedOfLight;
  );

ShowVKR[]:=Module[{},
  vrlist=Element["NAME","VR*"];
  vklist=Element["NAME","VK*"];
  zhlist=Element["NAME","ZHQC*"];
  zvlist=Element["NAME","ZVQC*"];

  brho=MASS*LINE["GAMMABETA","$$$"]/SpeedOfLight;
  k0=Element["K0",zhlist];
  sk0=Element["K0",zvlist];
  k1=Element["K1",vklist];
  sk1=Element["SK1",vklist];

  bl=k0*brho;
  sbl=sk0*brho;

  bdl=k1*brho;
  sbdl=sk1*brho;
  rot=Element["ROTATE",vrlist];
  dx=Element["DX",vklist];
  dy=Element["DY",vklist];
  $FORM="8.4";
  Print["            B1       A1     B2         A2     ROTATE    DX         DY"];
  Do[
    Print[vklist[[i]]," ",bl[[i]]," ",sbl[[i]]," ",bdl[[i]]," ",sbdl[[i]]," ",rot[[i]]," ",dx[[i]]," ",dy[[i]]];
    ,{i,1,Length[vklist]}];

  Print["|         |   B1 (T) |      A1 (T) |    B2 (T) |        A2 (T) |    ROTATE (rad) |   DX (m) |        DY (m) |"];
  Do[
    Print["| ",vklist[[i]]," | ",bl[[i]]," | ",sbl[[i]]," | ",bdl[[i]]," | ",sbdl[[i]]," | ",rot[[i]]," | ",dx[[i]]," | ",dy[[i]]," |"];
    ,{i,1,Length[vklist]}];

  ];

SetupBackleg[]:=Module[{tmp,aq},
(* backleg *)
  Library@Require["Optics/SuperKEKB/Backleg"];
  DefineBackleg[];

(* Q *)
  tmp=BacklegPS[#]&/@LINE["NAME","Q*"];
  aq=Select[tmp[[,1]],(#[1]=="A")&];

(* ZH/ZV *)

  Return[aq];
  ];

ReverseLine[]:=(
  FFS["USE ASCE; GEOCAL CELL EMIT CALC GEOFIX SAVE ALL"];
  Print["Before Reverse[CELL]:"];
  FFS["DISP IP* ALL", 6];
  
  bl$rev = -ExtractBeamLine[];

  ScanThread[(Element["CHI1", #1] = -#2)&,
    Element[{"NAME", "CHI1"}, "QC*"]];
  
  ScanThread[(Element["DZ", #1] = -#2)&,
    Element[{"NAME", "DZ"}, "QC*"]];
  
  ScanThread[(Element["DPX", #1] = -#2)&,
    Element[{"NAME", "DPX"}, "ES*"]];
  
  ScanThread[(Element["DPY", #1] = -#2)&,
    Element[{"NAME", "DPY"}, "ES*"]];

  FFS["SAVE ALL"];
  
  FFS["USE "//bl$rev];

  FFS["INS CALC"];
  
  Print["After Reverse[INS]:"];
  FFS["DISP IP* ALL", 6];
  );


dist[from_,to_]:=(
  LINE["S",to]-LINE["S",from]-LINE["L",from]*0.5+LINE["L",to]*0.5
  );

adjleng[c_,l_]:=Module[{dl,p},
  dl=(LINE["L",c]-l)*0.5;
  p=LINE["POSITION",c];
  If[LINE["TYPE",p-1]==1,
    LINE["L",p-1]+=dl;
    ,
    LINE["L",p-2]+=dl;
    ];
  If[LINE["TYPE",p+1]==1,
    LINE["L",p+1]+=dl;
    ,
    LINE["L",p+2]+=dl;
    ];
  LINE["L",c]=l;
  FFS["CALC",6];
  ];

adjdist[from_,to_,d_,fix_:1]:=Module[{d0,p,dl},
  d0=LINE["S",to]-LINE["S",from]-LINE["L",from]*0.5+LINE["L",to]*0.5;
  If[fix==2,
    p=LINE["POSITION",from];
    dl=d0-d;
    ,
    p=LINE["POSITION",to];
    dl=d-d0;
    ];
  If[LINE["TYPE",p-1]==1,
    LINE["L",p-1]+=dl;
    ,
    LINE["L",p-2]+=dl;
    ];
  If[LINE["TYPE",p+1]==1,
    LINE["L",p+1]-=dl;
    ,
    LINE["L",p+2]-=dl;
    ];
  FFS["CALC",6];
  ];

StartInjection[]:=(
  bl=ExtractBeamLine[BeamLineName[]];
  p=LINE["POSITION","INJECTIO"];
  bl=Join[Take[bl,{p,-1}],Take[bl,{1,p-1}]];
  FFS["USE bl"];
  FFS["CALC"];
  );

