GenParticles[npar_,kappa_:0]:=Module[{a,u,pu,v,pv,zz,dd,p,flag},
  emt=Emittance[Matrix->True];
  a=NormalCoordinates/.emt;
  StandardForm[$FORM="8.6";PageWidth=80;Print[a]];
  {emx,emy,emz}=Emittances/.emt;
  If[kappa>0,
    emy=kappa*emx;
    ];
  u=Sqrt[emx]*GaussRandom[npar];
  pu=-Sqrt[emx]*GaussRandom[npar];
  v=Sqrt[emy]*GaussRandom[npar];
  pv=-Sqrt[emy]*GaussRandom[npar];
  zz=Sqrt[emz]*GaussRandom[npar];
  dd=-Sqrt[emz]*GaussRandom[npar];
  p=(Inverse[a].#)&/@Thread[{u,pu,v,pv,zz,dd}];
  flag=Table[1,{npar}];
  {1,{p[[,1]],p[[,2]],p[[,3]],p[[,4]],p[[,5]],p[[,6]],flag}}
  ];

MoveScatPoint[beam_,p_]:=Module[{v,u},
  v=Thread[(mat2scat[[p]].#)&/@Thread[Drop[beam[[2]],-1]]];
  v[[1]]=v[[1]]+Twiss["DX",p];
  v[[2]]=v[[2]]+Twiss["DPX",p];
  v[[3]]=v[[3]]+Twiss["DY",p];
  v[[4]]=v[[4]]+Twiss["DPY",p];
  u=AppendTo[v,Table[1,{Length[beam[[2,7]]]}]];
  {p,u}
  ];


SetArray[beam_,w_]:=Module[{p,pref,ps,r,d},
  p=Sort[Select[Thread[{beam[[2,8]],w}],(#[[1]]>0)&]];
  pref=Union[p[[,1]]];
  d={};
  Do[
    ps=Select[p,(#[[1]]==pref[[i]])&];
    r=Plus@@ps[[,2]];
    AppendTo[d,{pref[[i]],r}];
    ,{i,1,Length[pref]}];
  d
  ];

GetAccept[b1_,b2_]:=Module[{px,py,dd},
  px=b1[[2,2]]*b2[[2,7]];
  py=b1[[2,4]]*b2[[2,7]];
  dd=b1[[2,6]]*b2[[2,7]];
  {b1[[1]],{Min[px],Max[px]},{Min[py],Max[py]},{Min[dd],Max[dd]}}
  ];

GetLostParticles[b1_,b2_,w_,region_:{2478,5014}]:=Module[{d},
  d=Select[Thread[{Table[b1[[1]],{Length[b1[[2,1]]]}],b2[[2,8]],w,b2[[2,9]],
    b1[[2,1]],b1[[2,2]],b1[[2,3]],b1[[2,4]],b1[[2,5]],b1[[2,6]],
    b2[[2,1]],b2[[2,2]],b2[[2,3]],b2[[2,4]],b2[[2,5]],b2[[2,6]]}],(#[[2]]>0)&];
  d=Sort[Select[d,(region[[1]]<#[[2]]<region[[2]])&],(#1[[2]]<#2[[2]])&];
  d
  ];

IRregion[]:=Module[{p},
  p=Thread[{Abs[LINE["S"]-LINE["S","IP"]],LINE["POSITION"]}];
  p=Select[p,(#[[1]]<30)&];
  {Min[p[[,2]]],Max[p[[,2]]]}
  ];

SaveFile[]:=Module[{},
!  Print["save"];
  d=StringReplace[date0,{"/"->""," "->"",":"->"_"}];
  file=RingID//proc//"_"//d[5,8]//"_"//d[1,4]//"_"//d[9,-1]//"-"//iseed//".dat";
  filep=RingID//proc//"_"//d[5,8]//"_"//d[1,4]//"_"//d[9,-1]//"-"//iseed//".plost";

  If[proc=="C",
    head={latticeFile,LINE["NAME","^^^"],{npar,thmax},{date0,DateString[]},{iseed,SeedRandom[]}};
    ];

  If[proc=="B",
    head={latticeFile,LINE["NAME","^^^"],{npar,umin},{date0,DateString[]},{iseed,SeedRandom[]}};
    ];
  
  If[proc=="T",
    head={latticeFile,LINE["NAME","^^^"],{npar,MINCOUP,PBUNCH},{date0,DateString[]},{iseed,SeedRandom[]}};
    ];

  f=OpenWrite[file];
  Write[f,head];
  Write[f,lossSum];
  Write[f,Thread[{pscat,lscat}]];
  Write[f,Thread[{pobs,lobs}]];
  Write[f,accept];
  Write[f,maskSet];
  Close[f];

  f=OpenWrite[filep];
  Write[f,lostParticles];
  Close[f];

  ];

Nb[bc_]:=Module[{},
  circ=LINE["S","$$$"]-(Twiss["DZ","$$$"]-Twiss["DZ","^^^"]);
  f0=SpeedOfLight/circ;
  bc/(ElectronCharge*f0)
  ];

ng[p_:1e-7]:=Module[{T=300,kb=1.38e-23},
  2*p/kb/T
  ];
