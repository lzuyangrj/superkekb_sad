# plot dynamic aperture LER
reset

set terminal postscript enhanced eps colour font 'Helvetica, 24' #set the terminal to output eps file
set key font ",24"
set key spacing 1.5
set border lw 2
set key right top #set position of the titles of points of lines

set output "DA_LER_PhaseII_noError_woBB.eps"
set xlabel "z/{/Symbol s}_z"
set ylabel "x/{/Symbol s}_x"
#set grid off
set xrange[-30:30]
set yrange[0:30]

plot 'DA_LER_Error_woApert.dat'  u 1:2 pt 5 ps 1.5 lc rgb 'red' lw 8 t "w/o Apert" w lp ,\
     'DA_LER_Error_Apert.dat'  u 1:2 pt 5 ps 1.5 lc rgb 'blue' lw 8 t "w Apert" w lp

# plot dynamic aperture HER
reset

set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file
set key font ",24"
set key spacing 1.5
set border lw 2
set key right top #set position of the titles of points of lines

set output "DA_HER_PhaseII_noError_woBB.eps"
set xlabel "z/{/Symbol s}_z"
set ylabel "x/{/Symbol s}_x"
#set grid off
set xrange[-20:20]
set yrange[0:20]

plot 'DA_HER_Error_woApert.dat'  u 1:2 pt 5 ps 1.5 lc rgb 'red' lw 8 t "w/o Apert" w lp ,\
     'DA_HER_Error_Apert.dat'  u 1:2 pt 5 ps 1.5 lc rgb 'blue' lw 8 t "w/ Apert" w lp