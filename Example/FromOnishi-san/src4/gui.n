
phaseFlag=If[phase==1,phase,phase-1];;
from="^^^";
to="$$$";
sym="QL*|B*";
normFlag=1;

comp111=KBFComponentFrame[f2,Add->{
  KBFRadioButton[Items->{"Phase-1","Phase-3"},Variable:>phaseFlag,Columns->3],
  KBFButton[Text->"Coulomb",Command:>(
    file1=KBFOpenDialog[".",RingID//"ERC*.dat"];
    wv4[State]="normal";
    ),WidgetOption->{Width->12},WidgetVariable:>wv1],
  KBFButton[Text->"Brems",Command:>(
    file2=KBFOpenDialog[".",RingID//"ERB*.dat"];
    wv4[State]="normal";
    ),WidgetOption->{Width->12},WidgetVariable:>wv2],
  KBFButton[Text->"Touschek",Command:>(
    file3=KBFOpenDialog[".",RingID//"ERT*.dat"];
    wv4[State]="normal";
    ),WidgetOption->{Width->12},WidgetVariable:>wv3],
  KBFButton[Text->"Do It",Command:>(
    TkReturn[];
    wv1[State]="disable";
    wv2[State]="disable";
    wv3[State]="disable";
    wv4[State]="disable";
    ),WidgetOption->{Width->12},WidgetVariable:>wv4],
  KBFSeparator[],
  KBFFrame[Add->{
    KBFString[Text->"from",Variable:>from,WidgetOption->{Width->12}],
    KBFString[Text->"to",Variable:>to,WidgetOption->{Width->12},NextColumn->True],
    KBFString[Text->"symbol",Variable:>sym,WidgetOption->{Width->12}],
    Null[]
    }],
  KBFCheckButton[Text->"Norm.",Variable:>normFlag,WidgetOption->{Width->8}],
  KBFButton[Text->"Coulomb",Command:>(coulo@norm=normFlag;coulo@op[from,to,sym]),WidgetOption->{Width->12},WidgetVariable:>wv5],
  KBFButton[Text->"Brems",Command:>(brems@norm=normFlag;brems@op[from,to,sym]),WidgetOption->{Width->12},WidgetVariable:>wv6],
  KBFButton[Text->"Touschek",Command:>(tousc@norm=normFlag;tousc@op[from,to,sym]),WidgetOption->{Width->12},WidgetVariable:>wv7],
  KBFSeparator[],
  KBFFrame[Add->{
    KBFNumber[Text->"Coulomb  [PPS]",Variable:>cpps,WidgetOption->{Width->10}],
    KBFNumber[Text->"Brems    [PPS]",Variable:>bpps,WidgetOption->{Width->10}],
    KBFNumber[Text->"Touschek [PPS]",Variable:>tpps,WidgetOption->{Width->10}],
    KBFNumber[Text->"R [1/s]",Variable:>crate,NumberForm->"10.4",WidgetOption->{Width->10},NextColumn->True],
    KBFNumber[Text->"R [1/s]",Variable:>brate,NumberForm->"10.4",WidgetOption->{Width->10}],
    KBFNumber[Text->"R [1/s]",Variable:>trate,NumberForm->"10.4",WidgetOption->{Width->10}]
    }],
  KBFSeparator[],
  KBFButton[Text->"Write Coulomb",Command:>(coulo@dumpLoss[]),WidgetOption->{Width->12},WidgetVariable:>wv8],
  KBFButton[Text->"Write Brems",Command:>(brems@dumpLoss[]),WidgetOption->{Width->12},WidgetVariable:>wv9],
  KBFButton[Text->"Write Touschek",Command:>(tousc@dumpLoss[]),WidgetOption->{Width->12},WidgetVariable:>wv10],
  Null[]
  }];

wv1[State]="normal";
wv2[State]="normal";
wv3[State]="normal";
wv4[State]="disable";
wv5[State]="disable";
wv6[State]="disable";
wv7[State]="disable";
wv8[State]="disable";
wv9[State]="disable";
wv10[State]="disable";

TkWait[];

If[phaseFlag==1,phase=1,phase=3];
