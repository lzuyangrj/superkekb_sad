# plot dynamic aperture LER
reset

set terminal postscript enhanced eps colour font 'Helvetica,22' #set the terminal to output eps file
set key font ",22"
set key spacing 1.5
set key right top #set position of the titles of points of lines

set output "DA_LER_PhaseII.eps"
set xlabel "z/{/Symbol s}_z"
set ylabel "x/{/Symbol s}_x"
#set grid off
set xrange[-30:30]
set yrange[0:30]

plot 'DA_LER_noError.dat'  u 1:2 pt 5 ps 1 lc rgb 'red' lw 5 t "w/o error+bb" w lp


# plot dynamic aperture HER
reset

set terminal postscript enhanced eps colour font 'Helvetica,22' #set the terminal to output eps file
set key font ",22"
set key spacing 1.5
set key right top #set position of the titles of points of lines

set output "DA_HER_PhaseII.eps"
set xlabel "z/{/Symbol s}_z"
set ylabel "x/{/Symbol s}_x"
#set grid off
set xrange[-30:30]
set yrange[0:20]

plot 'DA_HER_noError.dat'  u 1:2 pt 5 ps 1 lc rgb 'red' lw 5 t "w/o error+bb" w lp
