# SuperKEKB
2019.01.07
upload the files for Phase II DA study and scripts for BGS simulations
# questions
1) larger DX at "LBP4NP" and "BP1NRP" element (100 mm), reasonable?
2) tracking w/ Oide-san's SAD version has some bugs after executing "MakeBeamLine[]" (probaly due to longitudinal position shift)

# 2019-01-14
1) lib_func.h file was modified for BGS simulation; debug the error w/ matrixTwiss
2) BGS lifetime for HER: 202 mins

# 2018-01-17
1) for 500 turns tracking, BGS lifetime are:
   HER, 237 and 202 mins w/o and w/ collimators;
   LER, 183 and 130 mins w/o and w/ collimators   
2) ask Nakayama-san to check w/ his script
   PS, possible to check with analytical formulas (? to be done)

# 2018-01-17
1) set up "lib_gen.h" for halo simulation -> change the interesting range;
2) estimation of tracking time:
   2*tau_y/dt=
   where tau_y=46 ms is the damping time, dt=1.006e-5 s the revolution time.
   ==> 1.5e4 turns w/ data saving per 500 turns.

# 2018-01-24
1) fix the problem of disagreement between print-out nloss and actual particle loss (from the dis*.dat file)
2) fix the problem with "FractionalPart[]" in lib_func.h
3) BGS process stop at around 4500 turns on SAD server.. the reason is unknown.

# 2018-01-27
1) fix the bugs in InputDisBS[] function (to import previous BGS particles)
2) tracking with large amount particle are pretty slow at afsad1
   -> spllit 15000 turns tracking into 5 subtasks (3000 * 5)
3) check Parallezition in afsad1 --> no problem!
4) check COD/Twiss/Emittances w/ the same seed --> no problem, same parameters with a common seed (17).
5) creat "./functions/" folder under SuperKEKB for the functions
6) lifetime calculation with KEK SAD and Oide-san's SAD was found to be inconsistent (reason?).

# 2019-01-30
1) modify functions to obtain the extratracking after BGS generation/tracking;
2) BGS lifetime simulation finished, and is waiting for cross checking;

# 2020-05-03